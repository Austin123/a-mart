import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { ProductList, Cart } from "../src";
import { colors } from "../assets/themes/styles";
import { itemCountSelector } from "../src/ProductList/reducer";
import { connect } from "react-redux";

const Tab = createBottomTabNavigator();

const TabNavigation = (props) => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: colors.primary,
        unmountOnBlur: true,
      }}
    >
      <Tab.Screen
        name="home"
        component={ProductList}
        options={{
          title: "Home",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={30} />
          ),
          unmountOnBlur: true,
        }}
      />
      <Tab.Screen
        name="cart"
        component={Cart}
        options={{
          title: "Cart",
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="cart" color={color} size={30} />
          ),
          headerShown: false,
          unmountOnBlur: true,
          tabBarBadgeStyle: { backgroundColor: "orange", color: "#fff" },
          tabBarBadge: props.itemCount,
        }}
      />
    </Tab.Navigator>
  );
};

const mapStateToProps = (state) => ({
  itemCount: itemCountSelector(state),
});

const ConnectedTabNavigation = connect(mapStateToProps, {})(TabNavigation);

export { ConnectedTabNavigation as TabNavigation };
