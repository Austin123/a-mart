import React from "react";
import { connect } from "react-redux";
import { NavigationContainer } from "@react-navigation/native";
import { HomeNavigation } from "./HomeNavigation";

const Navigation = (props) => {
  return (
    <NavigationContainer>
      <HomeNavigation />
    </NavigationContainer>
  );
};

const ConnectedNavigation = connect()(Navigation);

export { ConnectedNavigation as Navigation };
