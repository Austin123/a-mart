# a-mart

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Austin123/a-mart.git
git branch -M main
git push -uf origin main
```

## How to run project

Create project with RN CLI and this app is for both iOS and android

After clone this repositories fire some commands for run the App

- yarn install
- yarn start or (you can directly run in individuals platform with below command)
- yarn ios or yarn android

- for local storage i have used redux with async storage.

```

```
