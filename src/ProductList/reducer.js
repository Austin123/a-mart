import {
  SAVE_CART_DATA,
  SAVE_DECREASE_COUNT,
  SAVE_DELETED,
  SAVE_INCREASE_COUNT,
  SAVE_ITEM_COUNT,
  SAVE_SUB_TOTAL,
  SAVE_TAX,
  SAVE_TOTAL,
} from "./actions";

// Initial State object for reducer and store
const initialState = {
  isLoading: false,
  error: null,
  itemCount: 0,
  cartData: [],
  increaseCount: 0,
  subTotal: 0,
  tax: 0,
  total: 0,
  deletedData: [],
};

// productList reducer with all the action types
const ProductListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SAVE_ITEM_COUNT:
      return {
        ...state,
        itemCount: payload,
      };
    case SAVE_CART_DATA:
      return {
        ...state,
        cartData: payload,
      };
    case SAVE_INCREASE_COUNT:
      return {
        ...state,
        increaseCount: payload,
      };
    case SAVE_SUB_TOTAL:
      return {
        ...state,
        subTotal: payload,
      };
    case SAVE_TAX:
      return {
        ...state,
        tax: payload,
      };
    case SAVE_TOTAL:
      return {
        ...state,
        total: payload,
      };
    case SAVE_DELETED:
      return {
        ...state,
        deletedData: payload,
      };

    default:
      return state;
  }
};

export default ProductListReducer;

export const itemCountSelector = (state) => state.productList.itemCount;
export const cartDataSelector = (state) => state.productList.cartData;
export const increaseCountSelector = (state) => state.productList.increaseCount;
export const subTotalSelector = (state) => state.productList.subTotal;
export const taxSelector = (state) => state.productList.tax;
export const totalSelector = (state) => state.productList.total;
export const deleteDataSelector = (state) => state.productList.deletedData;
