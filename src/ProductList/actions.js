const PRODUCT = "[Product]";
export const SAVE_CART_DATA = `${PRODUCT} Save cart data`;
export const SAVE_ITEM_COUNT = `${PRODUCT} Save item count`;
export const SAVE_INCREASE_COUNT = `${PRODUCT} Save increase count`;
export const SAVE_SUB_TOTAL = `${PRODUCT} Save sub total`;
export const SAVE_TAX = `${PRODUCT} Save tax`;
export const SAVE_TOTAL = `${PRODUCT} Save total`;
export const SAVE_DELETED = `${PRODUCT} Save deleted`;

export const setCartData = (data) => ({
  type: SAVE_CART_DATA,
  payload: data,
});

export const setItemCount = (data) => ({
  type: SAVE_ITEM_COUNT,
  payload: data,
});

export const setIncreaseCount = (data) => ({
  type: SAVE_INCREASE_COUNT,
  payload: data,
});

export const setSubTotal = (data) => ({
  type: SAVE_SUB_TOTAL,
  payload: data,
});

export const setTax = (data) => ({
  type: SAVE_TAX,
  payload: data,
});

export const setTotal = (data) => ({
  type: SAVE_TOTAL,
  payload: data,
});

export const setDeleted = (data) => ({
  type: SAVE_DELETED,
  payload: data,
});
