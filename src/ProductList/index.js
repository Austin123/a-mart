import React, { useState, useEffect, useCallback } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import { Loading } from "../../components";
import { colors } from "../../assets/themes/styles";
import { setCartData, setItemCount } from "./actions";
import { cartDataSelector, itemCountSelector } from "./reducer";

const DATA = [
  {
    id: 1,
    image: "https://m.media-amazon.com/images/I/41YknPIqAFL.jpg",
    name: "Raymond Shirt",
    price: "150.00",
  },
  {
    id: 2,
    image: "https://m.media-amazon.com/images/I/51fWdcNR3mL._SX466_.jpg",
    name: "Men's Digital Blue Checks Shirt Fabric 2.30m",
    price: "260.00",
  },
  {
    id: 3,
    image:
      "https://rukminim1.flixcart.com/image/714/857/kfwvcsw0-0/fabric/p/6/w/no-unstitched-az-393-avzira-original-imafw97jnhtchzue.jpeg?q=50",
    name: "Unstitched Polycotton Shirt Fabric Printed",
    price: "160.00",
  },
  {
    id: 4,
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTP6UszV81K52edT_RcShkdhGKrnvZHfVIcSA&usqp=CAU",
    name: "MEN TRENDY DESIGN DIGITAL PRINTED SHIRT FABRIC",
    price: "360.00",
  },
  {
    id: 5,
    image:
      "https://media.gettyimages.com/photos/close-up-of-a-mens-shirts-picture-id185270690?s=170667a",
    name: "Shirt set",
    price: "260.00",
  },
  {
    id: 6,
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDm1ckPVkTiH56SzL12pfEYOThNUHbE7FAzw&usqp=CAU",
    name: "Men Shirt Latest Design Shirt For Man Fancy Style Summer Wear",
    price: "660.00",
  },
  {
    id: 7,
    image:
      "https://rukminim1.flixcart.com/image/432/518/kn22m4w0/fabric/o/e/a/no-unstitched-hk-097-buhatrade-original-imagftaf2zgkg2ar.jpeg?q=70",
    name: "Men Shirt Latest Design",
    price: "232.00",
  },
  {
    id: 8,
    image: "https://cf.shopee.in/file/64c22279757b3fabff281fcf66495349_tn",
    name: "Men Shirt raymond",
    price: "140.00",
  },
  {
    id: 9,
    image: "https://m.media-amazon.com/images/I/81gFxZDDo-L._AC_UL320_.jpg",
    name: "Polycotton Shirts - Men's",
    price: "360.00",
  },
  {
    id: 10,
    image:
      "https://5.imimg.com/data5/ANDROID/Default/2021/1/MU/TX/OA/122361051/product-jpeg-500x500.jpg",
    name: "perfect n prime shirt fabric",
    price: "210.00",
  },
];

export const getProducts = () => {
  return DATA;
};
export const getProduct = (id) => {
  return DATA.find((data) => data.id == id);
};

const Item = ({ item, onAddToCartClick }) => {
  const navigation = useNavigation();

  const { image, price, name, id } = item;
  useEffect(() => {
    Image.prefetch(image);
  }, []);

  return (
    <View style={styles.card}>
      <View>
        <Image
          style={styles.tinyLogo}
          defaultSource={require("../../assets/images/defaultimage.png")}
          source={{
            uri: image,
            cache: "only-if-cached",
          }}
          resizeMode="contain"
          fadeDuration={300}
        />
      </View>
      <View>
        <View>
          <Text>{name}</Text>
        </View>
        <View style={styles.cartMainView}>
          <View style={{ width: "50%" }}>
            <Text>${price}</Text>
          </View>
          <TouchableOpacity
            style={{
              borderRadius: 5,
              padding: 8,
              backgroundColor: colors.primary,
            }}
            onPress={onAddToCartClick}
          >
            <Text style={{ color: "#fff", fontSize: 15 }}>Add to cart</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export const getProductTax = (price) => {
  const tax = (price * 1.26) / 100;
  return tax;
};

const ProductList = (props) => {
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [numColumns, setNumColumns] = useState(2);
  const navigation = useNavigation();
  const [items, setItems] = useState(props.cartData);

  const onAddToCartClick = (id) => {
    setRefreshing(true);
    const data = getProduct(id);
    try {
      props.setItemCount(getItemsCount());
    } catch (e) {
      console.log(e);
    }
    setItems((prevItems) => {
      const item = props.cartData?.find((item) => item.id == id);
      if (!item) {
        const newItemArr = [
          ...prevItems,
          {
            id,
            qty: 1,
            name: data.name,
            image: data.image,
            price: data.price,
            totalPrice: data.price,
            tax: getProductTax(Number(data.price)),
          },
        ];
        try {
          props.setCartData(newItemArr);
        } catch (e) {
          console.log(e);
        }
        return newItemArr;
      } else {
        const existingItemArr = props.cartData?.map((item) => {
          if (item.id == id) {
            item.qty++;
            item.name;
            item.image;
            item.price;
            item.totalPrice = Number(item.totalPrice) + Number(data.price);
            item.tax = Number(item.tax) + getProductTax(Number(data.price));
          }
          return item;
        });
        try {
          props.setCartData(existingItemArr);
        } catch (e) {
          console.log(e);
        }
        return existingItemArr;
      }
    });
  };

  const getItemsCount = () => {
    return items?.reduce((sum, item) => sum + item.qty, 0);
  };

  useEffect(() => {
    getItemsCount();
  }, []);

  const renderItem = useCallback(({ item }) => {
    return (
      <Item item={item} onAddToCartClick={() => onAddToCartClick(item.id)} />
    );
  }, []);

  const keyExtractor = useCallback((item) => item.id, []);

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1,
          }}
        >
          <Loading />
        </View>
      ) : (
        <>
          <FlatList
            data={DATA}
            extraData={refreshing}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            numColumns={numColumns}
            getItemLayout={(data, index) => ({
              length: 200,
              offset: 200 * index,
              index,
            })}
            removeClippedSubviews={true}
            initialNumToRender={5}
            windowSize={6}
            key={numColumns == 1 ? "#" : "*"}
          />
        </>
      )}
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  cartData: cartDataSelector(state),
  itemCount: itemCountSelector(state),
});

const ConnectedProductList = connect(mapStateToProps, {
  setItemCount,
  setCartData,
})(ProductList);

export { ConnectedProductList as ProductList };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  card: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    margin: 1,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.white,
  },
  tinyLogo: {
    width: 150,
    height: 200,
    padding: 10,
  },
  cartMainView: {
    paddingVertical: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
