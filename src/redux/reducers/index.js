// This file contains root reducer: it combines all other reducers
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

import ProductListReducer from "../../ProductList/reducer";

const profilePersistConfig = {
  key: "productList",
  storage: AsyncStorage,
  whitelist: [
    "itemCount",
    "cartData",
    "increaseCount",
    "subTotal",
    "tax",
    "total",
    "deletedData",
  ],
  stateReconciler: autoMergeLevel2,
};

const appReducer = combineReducers({
  productList: persistReducer(profilePersistConfig, ProductListReducer),
});

export default appReducer;
