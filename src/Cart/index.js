import React, { useCallback, useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  SafeAreaView,
} from "react-native";
import { connect } from "react-redux";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import { colors } from "../../assets/themes/styles";
import {
  setCartData,
  setDeleted,
  setIncreaseCount,
  setItemCount,
  setSubTotal,
  setTax,
  setTotal,
} from "../ProductList/actions";
import {
  cartDataSelector,
  deleteDataSelector,
  increaseCountSelector,
  subTotalSelector,
  taxSelector,
  totalSelector,
} from "../ProductList/reducer";
import { getProductTax } from "../ProductList";

const Cart = (props) => {
  const [cartArrayData, setCartArrayData] = useState(props.cartData);

  useEffect(() => {
    setCartArrayData(props.cartData);
  }, []);

  const renderItem = useCallback(({ item }) => {
    return (
      <View style={styles.cartMainView}>
        <View style={styles.productImageView}>
          <Image
            style={{
              height: 100,
              width: 90,
            }}
            source={{ uri: item.image }}
            resizeMode="contain"
          />
        </View>
        <View style={styles.namePriceDetailView}>
          <Text style={styles.nameText}>{item.name}</Text>
          <Text style={styles.priceText}>$ {item.price}</Text>
        </View>
        <View style={styles.increDecreeMainView}>
          <View style={styles.increDecreeView}>
            <TouchableOpacity onPress={() => incQuantity(item.id)}>
              <Text style={styles.increDecreeText}>+</Text>
            </TouchableOpacity>
            <Text style={styles.numberText}>{item.qty}</Text>
            <TouchableOpacity onPress={() => decQuantity(item.id)}>
              <Text style={styles.increDecreeText}>-</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            width: "20%",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => deleteItem(item.id)}>
            <MaterialCommunityIcons name="delete" color={"red"} size={30} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }, []);

  const ListFooterComponent = () => {
    return (
      <View style={styles.cartSummaryView}>
        <Text style={styles.nameText}>Cart Summary</Text>
        <View style={styles.summaryRowsView}>
          <Text style={{ fontSize: 18 }}>Sub Total:</Text>
          <Text style={styles.nameText}>$ {props.subTotal}</Text>
        </View>
        <View style={styles.summaryRowsView}>
          <Text style={{ fontSize: 18 }}>Tax:</Text>
          <Text style={styles.nameText}>$ {props.tax}</Text>
        </View>
        <View style={styles.summaryRowsView}>
          <Text style={{ fontSize: 18 }}>Shipping Fees:</Text>
          <Text style={styles.nameText}>$ 5</Text>
        </View>
        <View style={styles.summaryRowsView}>
          <Text style={{ fontSize: 18 }}>Total:</Text>
          <Text style={styles.nameText}>$ {props.total}</Text>
        </View>
        <TouchableOpacity
          style={styles.placeOrderButtonView}
          onPress={() => onPlaceOrderClick()}
        >
          <Text style={styles.placeOrderText}>Place Order</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const onPlaceOrderClick = () => {
    Alert.alert("Place Order", "Your order is placed successfully...", [
      {
        text: "OK",
        onPress: () => {
          props.setCartData([]);
          props.setItemCount(0);
          props.setDeleted([]);
          props.setIncreaseCount(0);
          props.setSubTotal(0);
          props.setTax(0);
          props.setTotal(0);
          setCartArrayData([]);
        },
      },
    ]);
  };

  const incQuantity = useCallback((id) => {
    const updatedQty = cartArrayData.filter((item) => {
      if (item.id === id) {
        props.increaseCount = item.qty++;
        item.totalPrice = Number(item.totalPrice) + Number(item?.price);
        item.tax = Number(item.tax) + getProductTax(Number(item?.price));
        try {
          props.setIncreaseCount(item.qty);
        } catch (e) {
          console.log(e);
        }
        try {
          props.setSubTotal(getSubTotalPrice());
        } catch (e) {
          console.log(e);
        }
        try {
          props.setTax(getTotalTax());
        } catch (e) {
          console.log(e);
        }
        try {
          props.setTotal(getTotalPrice());
        } catch (e) {
          console.log(e);
        }
        return item;
      }
      return cartArrayData;
    });
    setCartArrayData(updatedQty);
  }, []);

  const decQuantity = (id) => {
    const updatedQty = cartArrayData.filter((item) => {
      if (item.id === id) {
        if (item.qty > 0) {
          props.increaseCount = item.qty--;
          item.totalPrice = Number(item.totalPrice) - Number(item?.price);
          item.tax = Number(item.tax) - getProductTax(Number(item?.price));
          getSubTotalPrice();
          try {
            props.setIncreaseCount(item.qty);
          } catch (e) {
            console.log(e);
          }
          try {
            props.setSubTotal(getSubTotalPrice());
          } catch (e) {
            console.log(e);
          }
          try {
            props.setTax(getTotalTax());
          } catch (e) {
            console.log(e);
          }
          try {
            props.setTotal(getTotalPrice());
          } catch (e) {
            console.log(e);
          }
          return item;
        }
      }
      return cartArrayData;
    });

    setCartArrayData(updatedQty);
  };

  const getSubTotalPrice = () => {
    const subTotal = cartArrayData?.reduce(
      (sum, item) => Number(sum) + Number(item.totalPrice),
      0
    );
    try {
      props.setSubTotal(subTotal?.toFixed(2));
    } catch (e) {
      console.log(e);
    }
    return subTotal?.toFixed(2);
  };

  const getTotalTax = () => {
    const totalTax = cartArrayData?.reduce(
      (sum, item) => Number(sum) + Number(item.tax),
      0
    );
    try {
      props.setTax(totalTax?.toFixed(2));
    } catch (e) {
      console.log(e);
    }
    return totalTax?.toFixed(2);
  };

  const getTotalPrice = () => {
    const subTotal = getSubTotalPrice();
    const tax = getTotalTax();
    const total = Number(5) + Number(subTotal) + Number(tax);
    try {
      props.setTotal(total?.toFixed(2));
    } catch (e) {
      console.log(e);
    }
    return total?.toFixed(2);
  };

  const deleteItem = (id) => {
    Alert.alert("Cart Item", "Are you sure want to remove this item?", [
      {
        text: "Cancel",
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          const deletedItem = props.cartData.filter((item) => item.id !== id);
          try {
            props.setDeleted(deletedItem);
          } catch (e) {
            console.log(e);
          }
          setCartArrayData(deletedItem);
          if (props.deletedData.length < 0) props.setCartData([]);
        },
      },
    ]);
  };

  const deleteAllItem = () => {
    Alert.alert("Cart Item", "Are you sure want to remove all item?", [
      {
        text: "Cancel",
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          props.setCartData([]);
          props.setItemCount(0);
          props.setDeleted([]);
          props.setIncreaseCount(0);
          props.setSubTotal(0);
          props.setTax(0);
          props.setTotal(0);
          setCartArrayData([]);
        },
      },
    ]);
  };
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.cartHeader}>
        <View
          style={{
            width: "90%",
            alignItems: "center",
            paddingLeft: 30,
          }}
        >
          <Text style={styles.nameText}>Cart</Text>
        </View>
        {cartArrayData.length > 0 && (
          <TouchableOpacity
            onPress={() => deleteAllItem()}
            style={{ justifyContent: "flex-end" }}
          >
            <MaterialCommunityIcons name="delete" color={"red"} size={30} />
          </TouchableOpacity>
        )}
      </View>
      {cartArrayData.length > 0 ? (
        <FlatList
          data={cartArrayData}
          renderItem={renderItem}
          ListFooterComponent={ListFooterComponent}
        />
      ) : (
        <View
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Text style={styles.nameText}>Your cart is empty :(</Text>
        </View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  cartHeader: {
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 8,
    backgroundColor: colors.white,
  },
  cartMainView: {
    height: 120,
    flexDirection: "row",
    padding: 5,
    backgroundColor: colors.white,
    marginTop: 10,
  },
  productImageView: {
    width: "20%",
    alignItems: "center",
    paddingLeft: 10,
  },
  namePriceDetailView: {
    width: "55%",
    justifyContent: "center",
    paddingLeft: 20,
  },
  nameText: {
    fontSize: 18,
    fontWeight: "bold",
  },
  priceText: {
    fontSize: 16,
    marginTop: 8,
  },
  increDecreeMainView: {
    width: "17%",
    alignItems: "center",
    justifyContent: "center",
  },
  increDecreeText: {
    fontSize: 20,
    marginTop: 4,
    color: colors.white,
    fontWeight: "bold",
  },
  numberText: {
    fontSize: 18,
    marginTop: 5,
    color: "#fff",
  },
  increDecreeView: {
    height: 90,
    width: 40,
    borderRadius: 20,
    backgroundColor: colors.primary,
    alignItems: "center",
    justifyContent: "center",
  },
  cartSummaryView: {
    alignItems: "center",
    marginTop: 16,
    paddingTop: 10,
    backgroundColor: colors.white,
  },
  summaryRowsView: {
    borderBottomWidth: 1,
    height: 50,
    width: "100%",
    flexDirection: "row",
    marginTop: 6,
    justifyContent: "space-between",
    padding: 8,
  },
  placeOrderButtonView: {
    height: 50,
    width: "70%",
    borderRadius: 10,
    backgroundColor: colors.primary,
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  placeOrderText: {
    fontSize: 18,
    fontWeight: "bold",
    color: colors.white,
  },
});

const mapStateToProps = (state) => ({
  cartData: cartDataSelector(state),
  increaseCount: increaseCountSelector(state),
  subTotal: subTotalSelector(state),
  tax: taxSelector(state),
  total: totalSelector(state),
  deletedData: deleteDataSelector(state),
});

const ConnectedCartData = connect(mapStateToProps, {
  setIncreaseCount,
  setSubTotal,
  setTax,
  setTotal,
  setDeleted,
  setCartData,
  setItemCount,
})(Cart);

export { ConnectedCartData as Cart };
