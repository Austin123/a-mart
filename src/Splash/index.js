import { StyleSheet, Image, View } from "react-native";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { colors } from "../../assets/themes/styles";

const Splash = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace("tabNav");
    }, 3000);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require("../../assets/images/splash.jpeg")}
        resizeMode="contain"
      />
    </View>
  );
};

const ConnectedSplash = connect()(Splash);

export { ConnectedSplash as Splash };

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.primary,
  },
  image: {
    height: 200,
    width: 200,
  },
});
